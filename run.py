import os 
from image_reader import ImageReader


class MyProgram(object):

    def __init__(self):
        self.base_dir = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), 'files'
            )
        self.files = []

    def get_files(self, fpath):
        if os.path.isdir(fpath):
            fpath_dir_name = os.path.basename(fpath)
            if fpath_dir_name != 'generated':
                for f in os.listdir(fpath):
                    if os.path.isfile(f):
                        self.files.append(f)
                    else:
                        self.get_files(os.path.join(fpath, f))
        elif os.path.isfile(fpath):
            self.files.append(fpath)
                
    def run(self):
        self.get_files(self.base_dir)
        for f in self.files:
            if f.split('.')[-1] in ImageReader.valid_formats:
                ir = ImageReader(f)
                ir.read()


myprogram = MyProgram()
myprogram.run()