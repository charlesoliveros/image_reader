# Image Reader 0.1

Reads alphanumeric data from images files or pdf files with images using the OpenCV and PyTesseract libraries.

# Dependencies

```sh
$ sudo apt-get update
$ sudo apt-get install tesseract-ocr
$ sudo apt-get install libtesseract-dev
```
# Requirements

```sh
pip install -r requirements.txt
```

# How to use

Put the files to read in the "files" dir and execute the "run.py" file:

```sh
python run.py
```

It Will be generated a new dir with the name "generated" that contains the images and txt files created.

Author: [Carlos Oliveros](https://www.linkedin.com/in/carlos-oliveros-2a347358/)