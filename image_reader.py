from pdf2image import convert_from_path
import cv2
import pytesseract
from datetime import datetime
import os

class ImageReader(object):
    """
    Class to read and extract the alfanumeric information from images or pdf files with images.
    """
    valid_formats = ['jpg', 'jpeg', 'png', 'pdf',]

    def __init__(self, file_path, *args, **kwargs):
        # workdirs
        self.dir_generated = 'generated'
        # validations
        file_ext = file_path.split('.')[-1].lower()
        if not file_ext in self.valid_formats:
            raise Exception('Format {0} is not valid'.format(file_ext))
        # data file
        self.file_name =  os.path.basename(file_path)
        self.file_path = file_path
        self.file_ext = file_ext
        self.file_content = ''
        # image and txt files
        self.file_path_images = []
        self.file_path_txt = ''
        self.create_txt = kwargs.get('create_txt', True)
        self.execution_time = 0
            
    def conver_pdf_to_jpg(self):
        pages = convert_from_path(self.file_path, 500)
        file_path_image = self.get_file_path_generated(self.file_path)
        file_path_image = '{0}_NUMPAGE.jpg'.format(file_path_image)
        for num_page, page in enumerate(pages):
            file_path_image_page = file_path_image.replace('NUMPAGE', str(num_page))
            self.check_file_path(file_path_image_page)
            self.file_path_images.append(file_path_image_page)
            page.save(file_path_image_page, 'JPEG')
            print('File created: {0}'.format(file_path_image_page))   

    def get_file_path_generated(self, fpath):
        fpath_dir_name = os.path.dirname(fpath)
        path_generated = os.path.join(fpath_dir_name, self.dir_generated)
        if not os.path.exists(path_generated):
            os.makedirs(path_generated)
            print('Dir created: {0}'.format(path_generated))
        fpath_generated = os.path.join(path_generated, self.file_name)
        return fpath_generated

    def check_file_path(self, fpath):
        if os.path.exists(fpath):
            os.remove(fpath)
            print('File removed: {0}'.format(fpath))

    def read(self):
        time_init = datetime.now()
        print('Reading file {0}: '.format(self.file_path))
        if self.file_ext == 'pdf':
            self.conver_pdf_to_jpg()
        else:
            self.file_path_images.append(self.file_path[:])
        for file_path_image in self.file_path_images:
            img = cv2.imread(file_path_image)
            self.file_content += pytesseract.image_to_string(img)
        if self.create_txt:
            self.write_txt()
        #print('Content...\n', self.file_content)
        time_end = datetime.now()
        self.execution_time = time_end - time_init
        print('\nExecution time: {0}'.format(self.execution_time))
        print('='*100,'\n')

    def write_txt(self):
        self.file_path_txt = self.get_file_path_generated(self.file_path)
        self.file_path_txt = '{0}.txt'.format(self.file_path_txt)
        print('File created: {0}'.format(self.file_path_txt))
        f = open(self.file_path_txt, 'w+')
        f.write(self.file_content)
        f.close()